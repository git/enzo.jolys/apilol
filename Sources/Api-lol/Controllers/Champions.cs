﻿using Api_lol.Factories;
using DTO;
using DTO.DtoControlleur.Champions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using StubLib;
using System;
using System.Xml.Linq;

namespace Api_lol.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class Champions : Controller
    {
        private readonly IDataManager data;

        private readonly ILogger<Champions> logger;

        public Champions(ILogger<Champions> logger,IDataManager manager)
        {
            this.logger = logger;
            data = manager;
        }

        [HttpGet]
        public async Task<IActionResult> Get(int index = 0,int count = 0,string classe = "All")
        {
            IEnumerable<DtoChampions> champs = new List<DtoChampions>();

            try
            {
                if (count == 0)
                {
                    count = await data.ChampionsMgr.GetNbItems();
                }
                if (classe != "All")
                {
                    champs = (await data.ChampionsMgr.GetItems(index, count)).Where(e => e.Class.ToString() == classe).Select(Model => Model.ModelToDto());
                }
                else
                {
                    champs = (await data.ChampionsMgr.GetItems(index, count)).Select(Model => Model.ModelToDto());
                }
            }
            catch(Exception ex)
            {
                logger.LogInformation(ex.Message, ex);
                return BadRequest(ex.Message);
            }
            GetChampions dto = new GetChampions(champs.ToList(), index, count,await data.ChampionsMgr.GetNbItems());
            logger.LogInformation($"GetChampions: {dto}");
            return Ok(dto);
        }

        [HttpPost]
        public async Task<IActionResult> Post(DtoChampions champDTO)
        {
            Champion tmp = champDTO.DtoToModel();
            Champion champion;

            try
            {
                champion = await data.ChampionsMgr.AddItem(tmp);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message,champDTO);
                return BadRequest();
            }
            DtoChampions dtoChamp = champion.ModelToDto();
            logger.LogInformation("Post : Un Champion a été ajouté !", dtoChamp);
            return CreatedAtAction(nameof(GetChampion),new { name = dtoChamp.name},dtoChamp);
        }

        [HttpGet]
        [Route("{name}")]
        public async Task<IActionResult> GetChampion(string name)
        {
            Champion champion;

            try
            {
                champion = (await data.ChampionsMgr.GetItems(0, await data.ChampionsMgr.GetNbItems())).First(i => i.Name == name);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message, ex);
                return BadRequest(ex.Message);
            }
            if ( champion == null)
            {
                logger.LogInformation($"GetChampion : Le champion {name} n'a pas été trouvé !");
                return BadRequest();
            }
            DtoChampions result = champion.ModelToDto();
            return Ok(result);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteChampion(string name = "")
        {
            Champion champion;

            try
            {
                champion = (await data.ChampionsMgr.GetItems(0, await data.ChampionsMgr.GetNbItems())).First(e => e.Name == name);
            } catch (Exception ex)
            {
                logger.LogError(ex.Message, ex);
                return BadRequest(ex.Message);
            }
            await data.ChampionsMgr.DeleteItem(champion);
            logger.LogInformation($"Delete champio : suppression de {name}");
            return Ok(champion.ModelToDto());
        }

        [HttpGet]
        [Route("/GetItems")]
        public async Task<IEnumerable<DtoChampions?>> GetItems(int index,int count,string? propertyName = null, bool descending = false)
        {
            //logger.LogInformation($"{nameof(GetItems)}");
            return (await data.ChampionsMgr.GetItems(index, count, propertyName, descending)).Select(e => e.ModelToDto());
        }

        [HttpGet]
        [Route("/GetNbItems")]
        public async Task<int> GetNbItems()
        {
            //logger.LogInformation($"{nameof(GetNbItems)}");
            return await data.ChampionsMgr.GetNbItems();
        }
    }
}
