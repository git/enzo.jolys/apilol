﻿using Api_lol.Factories;
using DTO;
using DTO.DtoControlleur.Skins;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using System.Xml.Linq;

namespace Api_lol.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class Skins : Controller
    {
        private readonly IDataManager data;

        private readonly ILogger<Skins> logger;

        public Skins(ILogger<Skins> logger, IDataManager manager)
        {
            this.logger = logger;
            data = manager;
        }

        [HttpGet]
        public async Task<IActionResult> Get(int index = 0,int count = 0,float price = 0)
        {
            IEnumerable<DtoSkins> skins = new List<DtoSkins>();
            try
            {
                if (count == 0)
                {
                    count = await data.SkinsMgr.GetNbItems();
                }
                if ( price != 0 )
                {
                    skins = (await data.SkinsMgr.GetItems(index, count)).Where(e => e.Price == price).Select(Model => Model.ModelToDto());
                }
                else 
                {
                    skins = (await data.SkinsMgr.GetItems(index, count)).Select(Model => Model.ModelToDto());
                }
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
            GetSkins dto = new GetSkins(skins.ToList(),index,count,await data.SkinsMgr.GetNbItems());

            return Ok(dto);
        }

        [HttpGet]
        [Route("{name}")]
        public async Task<IActionResult> GetSkinsByName(string name)
        {
            Skin skin = (await data.SkinsMgr.GetItems(0, await data.ChampionsMgr.GetNbItems())).First(i => i.Name == name);
            if (skin == null)
            {
                return BadRequest();
            }
            DtoSkins result = skin.ModelToDto();
            return Ok(result);
        }

        [HttpGet]
        [Route("GetSkinsByChampionName/{name}")]
        public async Task<IActionResult> GetSkinsByChampionName(string name)
        {
            Champion champ = (await data.ChampionsMgr.GetItems(0, await data.ChampionsMgr.GetNbItems())).First(i => i.Name == name);
            List<Skin> skinsModele = new List<Skin>(champ.Skins);
            List<DtoSkins> skins = new List<DtoSkins>(skinsModele.Select(Model=> Model.ModelToDto()));
            if (skins == null)
            {
                return BadRequest();
            }
            return Ok(skins);
        }


        [HttpPost]
        [Route("/{skin}/For/{nameChampion}")]
        public async Task<IActionResult> Post(DtoSkins skin,string nameChampion)
        {
            Champion champion = (await data.ChampionsMgr.GetItems(0, await data.ChampionsMgr.GetNbItems())).First(i => i.Name == nameChampion);
            if (champion == null)
            {
                return BadRequest("Le champion n'existe pas !");
            }
            Skin skinModele = skin.DtoToModel(); 
            await data.SkinsMgr.AddItem(skinModele);
            return Ok();
        }

        [HttpPost]
        [Route("/GetItemsByChampion")]
        public async Task<IEnumerable<DtoSkins>> GetItemsByChampion(DtoChampions champ,int index,int count,string ordering = "null",bool descending = false)
        {
            return (await data.SkinsMgr.GetItemsByChampion(champ.DtoToModel(),index,count,ordering,descending)).Select(e => e.ModelToDto());
        }
    }
}
