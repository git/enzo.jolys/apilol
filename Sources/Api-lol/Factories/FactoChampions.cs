﻿using DTO;
using Model;
using System.Runtime.CompilerServices;

namespace Api_lol.Factories
{
    public static class FactoChampions
    {
        public static Champion DtoToModel(this DtoChampions champDto)
        {
            ChampionClass classe = ChampionClass.Unknown;
            switch (champDto.classe)
            {
                case "Assassin":
                    classe = ChampionClass.Assassin;
                    break;

                case "Fighter":
                    classe = ChampionClass.Fighter;
                    break;

                case "Mage":
                    classe = ChampionClass.Mage;
                    break;

                case "Support":
                    classe = ChampionClass.Support;
                    break;

                case "Tank":
                    classe = ChampionClass.Tank;
                    break;
            }
            return new Champion(champDto.name,champClass:classe,icon:champDto.icon,bio:champDto.bio,image:champDto.image);
        }

        public static DtoChampions ModelToDto(this Champion champ)
        {
            DtoChampions dto = new DtoChampions();

            dto.name = champ.Name;
            dto.classe = champ.Class.ToString();
            dto.bio = champ.Bio;
            dto.icon = champ.Icon;
            dto.image = champ.Image.Base64;

            return dto;
        }
    }
}
