﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class ClientChampions
    {
        private readonly HttpClient httpClient_client;
        private readonly string urlConstant = "https://localhost:7081/Champions/";

        public  ClientChampions()
        {
            this.httpClient_client = new HttpClient();
        }


        public async Task<List<DtoChampions>> Get()
        {
            var tmp = await httpClient_client.GetFromJsonAsync<List<DtoChampions>>(urlConstant);
            return tmp;
        }

        public async Task Post(DtoChampions champions)
        { 
            await httpClient_client.PostAsJsonAsync(urlConstant, champions);
        }

        public async Task<DtoChampions> GetChampion(string name)
        {
            var tmp = await httpClient_client.GetFromJsonAsync<DtoChampions>(urlConstant+name);
            return tmp;
        }
    }
}
