﻿using DTO;
using EntityFramwork.Factories;
using EntityFramwork;
using Model;
using StubLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Api_lol.Factories;
using Api_lol.Controllers;
using System.Data;

namespace Client
{
    public class StubEntityInit
    {
        private Factories facto = new Factories();
        private StubData data = new StubData();

        public async void AddAllChampions()
        {
            var liste = await data.ChampionsMgr.GetItems(0,await data.ChampionsMgr.GetNbItems());
            var listeDto = liste.ToList().Select(model => facto.ChampionModelToEntity(model));

            using (BDDContext db = new BDDContext())
            {
                foreach(var item in listeDto) 
                {
                    int ImageId = AddOneImage();
                    item.ImageId = ImageId;
                    db.Add(item);
                }
                db.SaveChanges();
            }
        }

        public async void AddAllSkins()
        {
            var skins = await data.SkinsMgr.GetItems(0, await data.SkinsMgr.GetNbItems());

            using (BDDContext db = new BDDContext())
            {
                foreach (var item in skins)
                {
                    int idChampion = (db.Champions.Where(m => m.Name == item.Champion.Name).First()).Id;
                    EntitySkins skin = facto.SkinsModelToEntity(item,idChampion);

                    int idImage = AddOneImage();
                    skin.ImageId = idImage;
                    db.Add(skin);
                }
                db.SaveChanges();
            }
        }

        public int AddOneImage()
        {
            Random aleatoire = new Random();
            EntityLargeImage tmpImage = new EntityLargeImage();
            tmpImage.Base64 = "Inconnu";
            tmpImage.Id = aleatoire.Next();

            using (BDDContext db = new BDDContext())
            {
                db.Add(tmpImage);
                db.SaveChanges();
            }
            return tmpImage.Id;
        }

        public async void AddAllRunes()
        {
            var runes = await data.RunesMgr.GetItems(0, await data.RunesMgr.GetNbItems());

            using (BDDContext db = new BDDContext())
            {
                foreach (var item in runes)
                {
                    EntityRunes rune = facto.RuneModelToEntity(item);

                    int idImage = AddOneImage();
                    rune.ImageId = idImage;
                    db.Add(rune);
                }
                db.SaveChanges();
            }
        }

        private void addSkill()
        {
           Factories factories = new Factories();

            List<Skill> list = new List<Skill>()
            { new Skill( "toto",SkillType.Unknown),
              new Skill("tata",SkillType.Unknown),
              new Skill("tutu",SkillType.Unknown)};

            EntitySkill skill1 = facto.SkillModeleToEntity(list[0],1);
            EntitySkill skill2 = facto.SkillModeleToEntity(list[1],1);
            EntitySkill skill3 = facto.SkillModeleToEntity(list[2],1);

            using (BDDContext db = new BDDContext())
            {
                db.Add(skill1);
                db.Add(skill2);
                db.Add(skill3);
                db.SaveChanges();
            }

        }

        public void Init()  
        {
            //Image en même temps que champion,rune,Skin

            //Champion 
            AddAllChampions();
            //Skin 
            AddAllSkins();
            //Rune
            AddAllRunes();
            //skill
            addSkill();
        }
    }
}
