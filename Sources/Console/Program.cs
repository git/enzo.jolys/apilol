﻿// See https://aka.ms/new-console-template for more information
using DTO;
using EntityFramwork.Manager;
using Model;
using System.Net.Http;
using System.Net.Http.Json;

Console.WriteLine("Start !");

Manager manager = new Manager();

//IEnumerable<Champion?> tmp = await manager.ChampionsMgr.GetItems(2, 4);
HttpClient http = new HttpClient();
Console.ReadLine();

IEnumerable<DtoChampions>  champions = await http.GetFromJsonAsync<IEnumerable<DtoChampions>>("https://localhost:7081/GetItems?index=0&count=6&descending=false");


Console.WriteLine(champions.ToList().FirstOrDefault().name);
Console.ReadLine();

Console.WriteLine("End !");