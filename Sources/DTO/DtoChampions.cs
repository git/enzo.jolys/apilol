﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DtoChampions
    {
        public string name { get; set; }

        public string classe { get; set; }

        public string bio { get; set; }
        public string icon { get; set; }

        public string image { get; set; }
    }
}   
