﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.DtoControlleur.Champions
{
    public class GetChampions
    {
        public List<DtoChampions> ListeChampions { get; set; }  

        public int Index { get; set; }
        public int Count { get; set; }
        public int TotalCount { get; set; }

        public GetChampions(List<DtoChampions> liste, int indexDto,int countDto,int totalCountDto)
        {
            Index = indexDto;
            Count = countDto;
            TotalCount= totalCountDto;
            ListeChampions = liste;
        }
    }
}
