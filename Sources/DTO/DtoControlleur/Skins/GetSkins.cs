﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.DtoControlleur.Skins
{
    public class GetSkins
    {
        public List<DtoSkins> ListeSkins { get; set; }

        public int Index { get; set; }
        public int Count { get; set; }
        public int TotalCount { get; set; }

        public GetSkins(List<DtoSkins> liste, int indexDto, int countDto, int totalCountDto)
        {
            Index = indexDto;
            Count = countDto;
            ListeSkins = liste;
            TotalCount = totalCountDto;
        }
    }
}
