﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DtoSkins
    {
        public string name { get; set; }

        public float price { get; set; }
        public string description { get; set; }

        public string icon { get; set; }

        public DtoChampions champion { get; set; }

        public string image { get; set; }
       
    }
}