﻿using DTO;
using Microsoft.EntityFrameworkCore;
using Model;
using static System.Net.Mime.MediaTypeNames;
using System.Diagnostics;
using System.Xml.Linq;
using System.ComponentModel;
using Model.enums;
using StubLib;
using EntityFramwork.Factories;

namespace EntityFramwork
{
    public class BDDContext : DbContext
    {
        
        public  BDDContext() { }
        public BDDContext(DbContextOptions<BDDContext> option) : base(option) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //création de la table Champion
            modelBuilder.Entity<EntityChampions>().HasKey(a => a.Id); 
            modelBuilder.Entity<EntityChampions>().Property(a => a.Id)
                                                  .ValueGeneratedOnAdd();
            modelBuilder.Entity<EntityChampions>().HasIndex(a => a.Name)
                                                  .IsUnique(true);

            //Clé avec skins
            modelBuilder.Entity<EntityChampions>()
                                                .HasMany(e => e.Skins)
                                                .WithOne(e => e.Champion)
                                                .HasForeignKey(x => x.ChampionId);
            //Clé avec skill
            modelBuilder.Entity<EntityChampions>()
                                                .HasMany(e => e.Skills)
                                                .WithOne(e => e.Champions)
                                                .HasForeignKey(x => x.ChampionId);
                                          
            // -------------------------------------------------------------------------------//
            //création de la table Skins
            modelBuilder.Entity<EntitySkins>().HasKey(m => m.Id); 
            modelBuilder.Entity<EntitySkins>().Property(m => m.Id)
                                           .ValueGeneratedOnAdd();

            // -------------------------------------------------------------------------------//
            //création de la table Images
            modelBuilder.Entity<EntityLargeImage>().HasKey(c => c.Id);
            modelBuilder.Entity<EntityLargeImage>().Property(c => c.Id)
                                                   .ValueGeneratedOnAdd();
            //Relation Images et Champion
            modelBuilder.Entity<EntityLargeImage>()
                                                .HasOne(e => e.Champion)
                                                .WithOne(z => z.Image)
                                                .HasForeignKey<EntityChampions>(x => x.ImageId);
            //Relation Images et Skins
            modelBuilder.Entity<EntityLargeImage>()
                                                .HasOne(e => e.Skin)
                                                .WithOne(z => z.Image)
                                                .HasForeignKey<EntitySkins>(x => x.ImageId);
            //Relation Images et Rune
            modelBuilder.Entity<EntityLargeImage>()
                                                .HasOne(e => e.Rune)
                                                .WithOne(z => z.Image)
                                                .HasForeignKey<EntityRunes>(x => x.ImageId);
            // -------------------------------------------------------------------------------//                                    
            //création de la table Runes
            modelBuilder.Entity<EntityRunes>().HasKey(c => c.Id);
            modelBuilder.Entity<EntityRunes>().Property(c => c.Id)
                                                   .ValueGeneratedOnAdd();

            modelBuilder.Entity<EntityRunes>().HasMany(e => e.CategorieRune)
                                               .WithOne(e => e.Rune)
                                               .HasForeignKey(e => e.IdRune);
            // -------------------------------------------------------------------------------//                                    
            //création de la table Skills
            modelBuilder.Entity<EntitySkill>().HasKey(c => c.Id);
            modelBuilder.Entity<EntitySkill>().Property(c => c.Id)
                                              .ValueGeneratedOnAdd();

            // -------------------------------------------------------------------------------//                                    
            //création de la table Page rune
            modelBuilder.Entity<EntityPageRune>().HasKey(c => c.Id);
            modelBuilder.Entity<EntityPageRune>().Property(c => c.Id)
                                              .ValueGeneratedOnAdd();

            modelBuilder.Entity<EntityPageRune>().HasMany(e => e.CategorieRune)
                                                   .WithOne(e => e.PageRune)
                                                   .HasForeignKey(e => e.IdPageRune);

            // -------------------------------------------------------------------------------//                                    
            //création de la table Categorie_Rune
            modelBuilder.Entity<EntityCategorieRune>().HasKey(c => c.Id);
            modelBuilder.Entity<EntityCategorieRune>().Property(c => c.Id)
                                              .ValueGeneratedOnAdd();

            // ---------------------------------- Stub --------------------------------------//

            DataStub dataStub = new DataStub();
            dataStub.InitSkins();

            List<EntityLargeImage> listImage = new List<EntityLargeImage>();
            List<EntityChampions> listChampions = new List<EntityChampions>();
            List<EntitySkins> listeSkins = new List<EntitySkins>();

            int i = 1;
            foreach (var champion in dataStub.champions)
            {
                listImage.Add(new EntityLargeImage() { Id = i, Base64 = champion.Image.Base64 });
                EntityChampions tmpChamp = champion.ChampionModelToEntity();
                tmpChamp.Id = i;
                tmpChamp.ImageId = i;
                listChampions.Add(tmpChamp);
                i++;
            }
            modelBuilder.Entity<EntityChampions>().HasData(listChampions);

            int c = 0;
            foreach(var skin in dataStub.skins)
            {
                listImage.Add(new EntityLargeImage() { Id = i,Base64 = skin.Image.Base64 });
                EntitySkins entitySkins = skin.SkinsModelToEntity();
                entitySkins.Id = c+1;
                entitySkins.ImageId = i;
                if ( c == 0 || c==1 || c == 2) { entitySkins.ChampionId = 1; }
                if (c == 3) { entitySkins.ChampionId =2; }
                if (c == 4 || c == 5 || c == 6) { entitySkins.ChampionId = 3; }
                if (c == 7 || c == 8 || c == 9) { entitySkins.ChampionId = 4; }
                if(c == 10 || c == 11 || c == 12) { entitySkins.ChampionId = 5; }
                if (c == 13 || c == 14 ) { entitySkins.ChampionId = 6; }
                if (c == 15 || c == 16) { entitySkins.ChampionId = 7; }

                listeSkins.Add(entitySkins);
                i++;
                c++;
            }
            modelBuilder.Entity<EntitySkins>().HasData(listeSkins);

            modelBuilder.Entity<EntityLargeImage>().HasData(listImage);

            /*
            // Ajout Image
            int nbImage = 29;
            List<EntityLargeImage> listImage = new List<EntityLargeImage>();
            for( int i=1; i<nbImage+1; i++)
            {
                EntityLargeImage tmpImage = new EntityLargeImage() { Id = i,Base64 = "Inconnu !" };
                listImage.Add(tmpImage);
            } 
            modelBuilder.Entity<EntityLargeImage>().HasData(listImage);
            // Ajout Champion

            List<EntityChampions> champions = new()
            {
                new EntityChampions() { Id = 1,Name = "Akali", ImageId = listImage[0].Id,Classe = ChampionClass.Assassin.ToString(),Bio = "Inconnu !",Icon = "Inconnu !"},
                new EntityChampions() { Id = 2, Name = "Aatrox",ImageId = listImage[1].Id, Classe = ChampionClass.Fighter.ToString(),Bio = "Inconnu !",Icon = "Inconnu !"},
                new EntityChampions() { Id = 3, Name = "Ahri",ImageId =  listImage[2].Id, Classe = ChampionClass.Mage.ToString(),Bio = "Inconnu !",Icon = "Inconnu !"},
                new EntityChampions() { Id = 4, Name = "Akshan",ImageId =  listImage[3].Id, Classe = ChampionClass.Marksman.ToString(),Bio = "Inconnu !",Icon = "Inconnu !"},
                new EntityChampions() { Id = 5, Name = "Bard",ImageId =  listImage[4].Id, Classe = ChampionClass.Support.ToString(),Bio = "Inconnu !",Icon = "Inconnu !"},
                new EntityChampions() { Id = 6, Name = "Alistar",ImageId =  listImage[5].Id, Classe = ChampionClass.Tank.ToString(),Bio = "Inconnu !",Icon = "Inconnu !"},
            };
            modelBuilder.Entity<EntityChampions>().HasData(champions);

            // Ajout de skin
            List<EntitySkins> skins = new()
            {
                new EntitySkins() {Id = 1,Name = "Stinger" ,ChampionId = champions[0].Id, Icon = "Inconnu !",Description = "Inconnu !",Price = 0,ImageId = listImage[6].Id},
                new EntitySkins() {Id = 2,Name = "Infernal" ,ChampionId = champions[0].Id, Icon = "Inconnu !",Description = "Inconnu !",Price = 0,ImageId = listImage[7].Id},
                new EntitySkins() {Id = 3,Name = "All-Star" ,ChampionId = champions[0].Id, Icon = "Inconnu !",Description = "Inconnu !",Price = 0,ImageId = listImage[8].Id},
                new EntitySkins() {Id = 4,Name = "Justicar" ,ChampionId = champions[1].Id, Icon = "Inconnu !",Description = "Inconnu !",Price = 0,ImageId = listImage[9].Id},
                new EntitySkins() {Id = 5,Name = "Mecha" ,ChampionId = champions[1].Id, Icon = "Inconnu !",Description = "Inconnu !",Price = 0,ImageId = listImage[10].Id},
                new EntitySkins() {Id = 6,Name = "Dynasty" ,ChampionId = champions[2].Id, Icon = "Inconnu !",Description = "Inconnu !",Price = 0,ImageId = listImage[11].Id},
                new EntitySkins() {Id = 7,Name = "Cyber Pop" ,ChampionId = champions[3].Id, Icon = "Inconnu !",Description = "Inconnu !",Price = 0,ImageId = listImage[12].Id},
                new EntitySkins() {Id = 8,Name = "Elderwood" ,ChampionId = champions[4].Id, Icon = "Inconnu !",Description = "Inconnu !",Price = 0,ImageId = listImage[13].Id},
                new EntitySkins() {Id = 9,Name = "Black" ,ChampionId = champions[5].Id, Icon = "Inconnu !",Description = "Inconnu !",Price = 0,ImageId = listImage[14].Id},
            };
            modelBuilder.Entity<EntitySkins>().HasData(skins);

            // Ajout skills
            List<EntitySkill> skills = new()
            {
                new EntitySkill() {Id = 1,ChampionId = champions[0].Id ,Type = SkillType.Basic.ToString(),Name = "Inconnu !",Description = "Inconnu !"},
                new EntitySkill() {Id = 2,ChampionId = champions[1].Id ,Type = SkillType.Ultimate.ToString(),Name = "Inconnu !",Description = "Inconnu !"},
                new EntitySkill() {Id = 3,ChampionId = champions[2].Id ,Type = SkillType.Unknown.ToString(),Name = "Inconnu !",Description = "Inconnu !"},
            };
            modelBuilder.Entity<EntitySkill>().HasData(skills);

            // Ajout Rune
            List<EntityRunes> runes = new()
            {
                new EntityRunes() { Id = 1,Family = RuneFamily.Precision.ToString(),ImageId = listImage[15].Id,Name = "Inconnu !",Description = "Inconnu !",Icon = "Inconnu !"},
                new EntityRunes() { Id = 2,Family = RuneFamily.Domination.ToString(),ImageId = listImage[16].Id,Name = "Inconnu !",Description = "Inconnu !",Icon = "Inconnu !"},
                new EntityRunes() { Id = 3,Family = RuneFamily.Unknown.ToString(),ImageId = listImage[17].Id,Name = "Inconnu !",Description = "Inconnu !",Icon = "Inconnu !"},
            };
            modelBuilder.Entity<EntityRunes>().HasData(runes);

            // Ajout Rune Page
            List<EntityPageRune> pageRune = new()
            {
                new EntityPageRune() { Id = 1,Name = "Inconnu !"},
                new EntityPageRune() { Id = 2,Name = "Inconnu !"},
                new EntityPageRune() { Id = 3,Name = "Inconnu !"},
            };
            modelBuilder.Entity<EntityPageRune>().HasData(pageRune);

            // Ajout Categorie Rune
            List<EntityCategorieRune> categorieRune = new()
            {
                new EntityCategorieRune() { Id = 1,Category = CategoriePageRune.Major.ToString(),IdPageRune = pageRune[0].Id , IdRune = runes[0].Id },
                new EntityCategorieRune() { Id = 2,Category = CategoriePageRune.Minor.ToString(),IdPageRune = pageRune[1].Id , IdRune = runes[1].Id },
                new EntityCategorieRune() { Id = 3,Category = CategoriePageRune.Other.ToString(),IdPageRune = pageRune[2].Id , IdRune = runes[2].Id },
            };
            modelBuilder.Entity<EntityCategorieRune>().HasData(categorieRune);
            */
        }

        public DbSet<EntityChampions> Champions { get; set; }
        public DbSet<EntitySkins> Skins { get; set; }
        public DbSet<EntityLargeImage> Images { get; set; }
        public DbSet<EntityRunes> Runes { get; set; }
        public DbSet<EntitySkill> Skills { get; set; }
        public DbSet<EntityPageRune> PageRunes { get; set; }
        public DbSet<EntityCategorieRune> CategorieRunes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            //optionsBuilder.UseSqlite($"Data Source=BDD-APILOL.db");
            optionsBuilder.UseSqlite($"Data Source=C:\\Users\\Jolys Enzo\\home\\BUT\\Projet\\TMP\\Api-LOL\\Sources\\EntityFramwork\\BDD-APILOL.db");
            }
        }
    }
}