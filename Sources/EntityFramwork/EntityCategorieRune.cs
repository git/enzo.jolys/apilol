﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramwork
{
    public class EntityCategorieRune
    {
        public int Id { get; set; }

        public string Category { get; set; }

        // Relation Page rune
        public int IdPageRune { get; set;}
        public EntityPageRune PageRune { get; set; }

        // Relation Rune
        public int IdRune { get; set;}
        public EntityRunes Rune { get; set; }
    }
}
