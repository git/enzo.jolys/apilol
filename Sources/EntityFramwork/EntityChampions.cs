﻿using EntityFramwork;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramwork
{
    public class EntityChampions
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Bio { get; set; }

        public string Icon { get; set; }
        public string Classe { get; set; } 

        // ----------------- Skin -----------------------//
        public List<EntitySkins> Skins { get; set; }

        // ----------------- Image -----------------------//
        public int ImageId { get; set; }
        public EntityLargeImage Image { get; set; }

        // ------------------- Skill ----------------------//
        public List<EntitySkill> Skills { get; set; }

    }
}
