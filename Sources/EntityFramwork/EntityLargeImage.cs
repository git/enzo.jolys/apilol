﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramwork
{
    public class EntityLargeImage
    {
        public int Id { get; set; }

        public string Base64 { get; set; }

        // --------- Champion ------------ //
        public EntityChampions Champion { get; set; }

        // --------- Skin ------------ //
        public EntitySkins Skin { get; set; }
        // --------- Rune ------------ //
        public EntityRunes Rune { get; set; }
    }
}
