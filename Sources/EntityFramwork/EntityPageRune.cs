﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramwork
{
    public class EntityPageRune
    {
        public int Id { get; set; }

        public string Name { get; set; }

        // Relation 
        public List<EntityCategorieRune> CategorieRune { get; set;
        }
    }
}
