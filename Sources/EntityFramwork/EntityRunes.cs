﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramwork
{
    public class EntityRunes
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public string Family { get; set; }

        // --------- Images ---------- //
        public int ImageId { get; set; }
        public EntityLargeImage Image { get; set; }

        // -------- CategorieRune --------//

        public List<EntityCategorieRune> CategorieRune { get; set; }
    }
}
