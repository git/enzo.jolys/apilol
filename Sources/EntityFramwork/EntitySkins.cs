﻿using DTO;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramwork
{
    public class EntitySkins
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public string Icon { get; set; }

        public float Price { get; set; }
        
        // ----------- Champion ----------- //
        public int ChampionId { get; set; }
        public EntityChampions Champion { get; set; }

        // ----------- Image ------------ //
        public int ImageId { get; set; }
        public EntityLargeImage Image { get; set; }
    }
}
