﻿using DTO;
using Model;
using static System.Net.Mime.MediaTypeNames;
using System.Threading.Tasks;

namespace EntityFramwork.Factories
{
    public static class Factories
    {

        // Factorie Champion
        public static EntityChampions ChampionModelToEntity(this Champion champ)
        {
            EntityChampions entity = new EntityChampions();

            entity.Name = champ.Name;
            entity.Bio = champ.Bio;
            entity.Icon = champ.Icon;
            entity.Classe = champ.Class.ToString();

            return entity;
        }

        public static Champion EntityChampionToModele(this EntityChampions entity) 
        {
            ChampionClass classe = ChampionClass.Unknown;
            string image;

            switch (entity.Classe)
            {
                case "Assassin":
                    classe = ChampionClass.Assassin;
                    break;

                case "Fighter":
                    classe = ChampionClass.Fighter;
                    break;

                case "Mage":
                    classe = ChampionClass.Mage;
                    break;

                case "Support":
                    classe = ChampionClass.Support;
                    break;

                case "Tank":
                    classe = ChampionClass.Tank;
                    break;
            }
            using (BDDContext db = new BDDContext())
            {
                image = db.Images.Where(e => e.Id == entity.ImageId).First().Base64;
            }

            return new Champion(entity.Name,champClass:classe,icon:entity.Icon,bio:entity.Bio,image:image);
        }

        // Skins
        public static EntitySkins SkinsModelToEntity(this Skin skin)
        {
            EntitySkins entity= new EntitySkins();

            entity.Price = skin.Price;
            entity.Icon = skin.Icon;
            entity.Name = skin.Name;
            entity.Description = skin.Description;

            return entity;
        }

        public static Skin SkinEntityToModele(this EntitySkins entity)
        {
            Champion champ;
            LargeImage image;
            using (BDDContext db = new BDDContext())
            {
                champ = db.Champions.Where(e => e.Id == entity.ChampionId).First().EntityChampionToModele();
                image = db.Images.Where(e => e.Id == entity.ImageId).First().ImageEntityToModele();
            }
            return new Skin(entity.Name, champ, entity.Price, entity.Icon, image.Base64, entity.Description);
        }


        public static EntityRunes RuneModelToEntity(this Rune rune)
        {
            EntityRunes entity = new EntityRunes();

            entity.Name= rune.Name;
            entity.Icon= rune.Icon;
            entity.Description = rune.Description;
            entity.Family = rune.Family.ToString();
            
            return entity;
        }

        public static EntitySkill SkillModeleToEntity(this Skill skill,int championId)
        {
            EntitySkill entity = new EntitySkill();

            entity.Name = skill.Name;
            entity.Description = skill.Description;
            entity.Type = skill.Type.ToString();
            entity.ChampionId = championId;

            return entity;
        }

        public static LargeImage ImageEntityToModele(this EntityLargeImage entity)
        {
            return new LargeImage(entity.Base64);
        }
    }
}
