﻿using Model;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramwork.Manager
{
    public partial class Manager : IDataManager
    {
        public Manager() 
        {
            ChampionsMgr = new ManagerChampion();
            SkinsMgr = new ManagerSkins();
            RunesMgr = new ManagerRune();
            RunePagesMgr = new ManagerRunePage();
        }

        public IChampionsManager ChampionsMgr { get; }

        public ISkinsManager SkinsMgr { get; }

        public IRunesManager RunesMgr { get; }

        public IRunePagesManager RunePagesMgr { get; }
    }

}