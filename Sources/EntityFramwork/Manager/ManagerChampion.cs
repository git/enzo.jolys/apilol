﻿using EntityFramwork.Factories;
using Model;

namespace EntityFramwork.Manager
{
    public class ManagerChampion : IChampionsManager
    {

        public Task<Champion?> AddItem(Champion? item)
        {
            try
            {
                using (BDDContext db = new BDDContext())
                {
                    db.Champions.Add(item.ChampionModelToEntity());
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return Task.FromResult<Champion?>(null);
            }
            return Task.FromResult<Champion?>(item);
        }

        public Task<bool> DeleteItem(Champion? item)
        {
            if(item == null)
            {
                return Task.FromResult(false);
            }
            using (BDDContext db = new BDDContext())
            {
                EntityChampions ?entityChamp = db.Champions.Where(e => e.Name == item.Name).FirstOrDefault();
                if ( entityChamp == null)
                {
                    return Task.FromResult(false);
                }
                db.Champions.Remove(entityChamp);
                db.SaveChanges();
            }
            return Task.FromResult(true);
        }

        public Task<IEnumerable<Champion?>> GetItems(int index, int count, string? orderingPropertyName = null, bool descending = false)
        {
            IEnumerable<Champion?> items = new List<Champion>();

            using (BDDContext db = new BDDContext())
            {
                if ( descending == false)
                {
                    items = db.Champions.Skip(index).Take(count).OrderBy(e => e.Name).Select(e => e.EntityChampionToModele()).ToList();
                }
                else
                {
                    items = db.Champions.Skip(index).Take(count).OrderByDescending(e => e.Name).Select(e => e.EntityChampionToModele()).ToList();
                }
            }
            return Task.FromResult(items);
        }

        public Task<IEnumerable<Champion?>> GetItemsByCharacteristic(string charName, int index, int count, string? orderingPropertyName = null, bool descending = false)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Champion?>> GetItemsByClass(ChampionClass championClass, int index, int count, string? orderingPropertyName = null, bool descending = false)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Champion?>> GetItemsByName(string substring, int index, int count, string? orderingPropertyName = null, bool descending = false)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Champion?>> GetItemsByRunePage(RunePage? runePage, int index, int count, string? orderingPropertyName = null, bool descending = false)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Champion?>> GetItemsBySkill(Skill? skill, int index, int count, string? orderingPropertyName = null, bool descending = false)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Champion?>> GetItemsBySkill(string skill, int index, int count, string? orderingPropertyName = null, bool descending = false)
        {
            throw new NotImplementedException();
        }

        public Task<int> GetNbItems()
        {
            int nb = 0;

            using (BDDContext db = new BDDContext())
            {
                nb = db.Champions.Count();
            }
            return Task.FromResult(nb);
        }

        public Task<int> GetNbItemsByCharacteristic(string charName)
        {
            throw new NotImplementedException();
        }

        public Task<int> GetNbItemsByClass(ChampionClass championClass)
        {
            throw new NotImplementedException();
        }

        public Task<int> GetNbItemsByName(string substring)
        {
            throw new NotImplementedException();
        }

        public Task<int> GetNbItemsByRunePage(RunePage? runePage)
        {
            throw new NotImplementedException();
        }

        public Task<int> GetNbItemsBySkill(Skill? skill)
        {
            throw new NotImplementedException();
        }

        public Task<int> GetNbItemsBySkill(string skill)
        {
            throw new NotImplementedException();
        }

        public Task<Champion?> UpdateItem(Champion? oldItem, Champion? newItem)
        {
            throw new NotImplementedException();
        }
    }
}
