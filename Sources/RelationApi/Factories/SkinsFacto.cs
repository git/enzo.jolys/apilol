﻿using DTO;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RelationApi.Factories
{
    public static class SkinsFacto
    {
        public static DtoSkins ModelToDto(this Skin skin)
        {
            DtoSkins dto = new DtoSkins();

            dto.name = skin.Name;
            dto.description = skin.Description;
            dto.price = skin.Price;
            dto.icon = skin.Icon;
            dto.image = skin.Image.Base64;
            //dto.champion = skin.Champion.ModelToDto();

            return dto;
        }

        public static Skin DtoToModel(this DtoSkins skinDto)
        {
            return new Skin(skinDto.name, skinDto.champion.DtoToModel());
        }
    }
}
