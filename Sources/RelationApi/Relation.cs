﻿
using Model;

namespace RelationApi
{
    
    public class Relation : IDataManager
    {
        private readonly string ipApi = "https://localhost:7081/";
        static HttpClient http = new HttpClient();

        public Relation()
        {
            ChampionsMgr = new RelationChampion(ipApi,http);
            SkinsMgr = new RelationSkins(ipApi,http);
            RunesMgr = new RelationRune(ipApi,http);
            RunePagesMgr = new RelationRunePage(ipApi,http);
        }

        public IChampionsManager ChampionsMgr { get; }

        public ISkinsManager SkinsMgr { get; }

        public IRunesManager RunesMgr { get; }

        public IRunePagesManager RunePagesMgr { get; }

    }
}