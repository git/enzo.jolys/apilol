using Api_lol.Controllers;
using DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StubLib;
using System.Net.Http.Json;
using System.Reflection;

namespace TestUnitaireApi
{
    public class TestChampions
    {
        StubData stubChamps;
        Champions controlleurChampion;
        
        public TestChampions() 
        {
            this.stubChamps = new StubData();
            this.controlleurChampion = new Champions(null,stubChamps);
        }


        [Fact]
        public async Task TestGetChampions()
        {
            IEnumerable<DtoChampions?> liste = await controlleurChampion.GetItems(0,3);            

            if (liste == null)
            {
                Assert.Fail("Liste okObject == null");
                return;
            }
            Assert.Equal(3,liste.Count());
        }

        [Fact]
        public async Task TestGetNbChampions()
        {
            int nb = await controlleurChampion.GetNbItems();

            Assert.Equal(6,nb);
        }
    }
}