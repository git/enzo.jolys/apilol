using DTO;
using EntityFramwork;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Model;

namespace TestUnitaireBDD
{
    public class TestChampions
    {
        private void Add_Champion_Image(DbContextOptions<BDDContext> options)
        {
            
            //Image 1
            Random aleatoire = new Random();
            EntityLargeImage tmpImage = new EntityLargeImage();
            tmpImage.Base64 = "Inconnu";
            tmpImage.Id = aleatoire.Next();

            //Image 2
            EntityLargeImage tmpImage2 = new EntityLargeImage();
            tmpImage2.Base64 = "Inconnu";
            tmpImage2.Id = aleatoire.Next();

            //Image 2
            EntityLargeImage tmpImage3 = new EntityLargeImage();
            tmpImage3.Base64 = "Inconnu";
            tmpImage3.Id = aleatoire.Next();

            //prepares the database with one instance of the context
            using (var context = new BDDContext(options))
            {
                //context.Database.OpenConnection();    
                context.Database.EnsureCreated();

                EntityChampions chewie = new EntityChampions { Name = "Chewbacca", Bio = "Inconnu", Icon = "Inconnu", Classe = ChampionClass.Assassin.ToString() };
                EntityChampions yoda = new EntityChampions { Name = "Yoda", Bio = "Inconnu", Icon = "Inconnu", Classe = ChampionClass.Assassin.ToString() };
                EntityChampions ewok = new EntityChampions { Name = "Ewok", Bio = "Inconnu", Icon = "Inconnu", Classe = ChampionClass.Assassin.ToString() };

                context.Add(tmpImage);
                context.Add(tmpImage2);
                context.Add(tmpImage3);
                chewie.ImageId = tmpImage.Id;
                yoda.ImageId = tmpImage2.Id;
                ewok.ImageId = tmpImage3.Id;
                context.Add(chewie);
                context.Add(yoda);
                context.Add(ewok);
                context.SaveChanges();
            }
        }



        [Fact]
        public void Add_Test()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<BDDContext>()
                .UseSqlite(connection)
                .Options;

            Add_Champion_Image(options);

            //uses another instance of the context to do the tests
            using (var context = new BDDContext(options))
            {
                context.Database.EnsureCreated();

                Assert.Equal(10, context.Champions.Count());
                Assert.Equal("Renekton", context.Champions.First().Name);
            }
        }

        [Fact]
        public void Modify_Test()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<BDDContext>()
                .UseSqlite(connection)
                .Options;

            Add_Champion_Image(options);

            //uses another instance of the context to do the tests
            using (var context = new BDDContext(options))
            {
                context.Database.EnsureCreated();

                Assert.Equal(1, context.Champions.Where(e => e.Name == "Ewok").Count());
                var ewok = context.Champions.Where(n => n.Name == "Ewok").First();
                ewok.Name = "Wicket";
                context.SaveChanges();
            }

            //uses another instance of the context to do the tests
            using (var context = new BDDContext(options))
            {
                context.Database.EnsureCreated();

                string nameToFind = "Wicket";
                Assert.Equal(1, context.Champions.Where(n => n.Name == nameToFind).Count());
            }
        }

        [Fact]
        public void Delete_Test()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<BDDContext>()
                .UseSqlite(connection)
                .Options;

            Add_Champion_Image(options);

            using (var context = new BDDContext(options))
            {
                EntityChampions tmpChamp = context.Champions.OrderBy(e => e.Name).Include(e => e.Image).Last();

                context.Remove(tmpChamp);
                context.SaveChanges();
            }

            using (var context = new BDDContext(options))
            {
                Assert.Equal(9, context.Champions.Count());
                Assert.Equal("Yoda",context.Champions.OrderBy(e => e.Name).Last().Name);
            }
        }
    }
}