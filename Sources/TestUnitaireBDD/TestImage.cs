﻿using DTO;
using EntityFramwork;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestUnitaireBDD
{
    public class TestImage
    {
        private void Add_Image(DbContextOptions<BDDContext> options)
        {
            
            EntityLargeImage tmpImage = new EntityLargeImage();
            tmpImage.Id = 97;
            tmpImage.Base64 = "Inconnu";

            EntityLargeImage tmpImage2 = new EntityLargeImage();
            tmpImage2.Id = 98;
            tmpImage2.Base64 = "Inconnu";

            EntityLargeImage tmpImage3 = new EntityLargeImage();
            tmpImage3.Id = 99;
            tmpImage3.Base64 = "Inconnu";

            //prepares the database with one instance of the context
            using (var context = new BDDContext(options))
            {
                //context.Database.OpenConnection();    
                context.Database.EnsureCreated();

                context.Add(tmpImage);
                context.Add(tmpImage2);
                context.Add(tmpImage3);
                context.SaveChanges();
            }
           
            
        }

        [Fact]
        public void Add_Test() 
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<BDDContext>()
                .UseSqlite(connection)
                .Options;

            Add_Image(options);

            using (var context = new BDDContext(options))
            {
                Assert.Equal(27,context.Images.Count());
                Assert.Equal(1,context.Images.First().Id);
            }
        }

        [Fact]
        public void Modify_Test()
        {
            
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<BDDContext>()
                .UseSqlite(connection)
                .Options;

            Add_Image(options);

            using (var context = new BDDContext(options))
            {
                var image = context.Images.Where(e => e.Id == 97).First();
                image.Base64 = "Modify";
                context.SaveChanges();
            }

            using (var context = new BDDContext(options))
            {
                var image = context.Images.Where(e => e.Base64 == "Modify").First();
                Assert.NotNull(image);
            }
        }

        [Fact]
        public void Delete_Test()
        {
            
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<BDDContext>()
                .UseSqlite(connection)
                .Options;

            Add_Image(options);

            using (var context = new BDDContext(options))
            {
                var image = context.Images.Where(e => e.Id == 97).First();
                context.Remove(image);
                context.SaveChanges();
            }
            using (var context = new BDDContext(options))
            {
                Assert.Equal(26,context.Images.Count());
            }
        }
          
    }
}
