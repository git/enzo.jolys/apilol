﻿using DTO;
using EntityFramwork;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestUnitaireBDD
{
    public class TestRunes
    {

        private void Add_Champion_Rune(DbContextOptions<BDDContext> options)
        {
            //Image 1
            Random aleatoire = new Random();
            EntityLargeImage tmpImage = new EntityLargeImage();
            tmpImage.Base64 = "Inconnu";
            tmpImage.Id = aleatoire.Next();

            //Image 2
            EntityLargeImage tmpImage2 = new EntityLargeImage();
            tmpImage2.Base64 = "Inconnu";
            tmpImage2.Id = aleatoire.Next();

            //Image 2
            EntityLargeImage tmpImage3 = new EntityLargeImage();
            tmpImage3.Base64 = "Inconnu";
            tmpImage3.Id = aleatoire.Next();

            //prepares the database with one instance of the context
            using (var context = new BDDContext(options))
            {
                //context.Database.OpenConnection();    
                context.Database.EnsureCreated();

                EntityRunes chewie = new EntityRunes { Name = "Chewbacca", Icon = "Inconnu",Description = "Inconnu",Family = RuneFamily.Unknown.ToString()};
                EntityRunes yoda = new EntityRunes { Name = "Yoda",Icon = "Inconnu", Description = "Inconnu", Family = RuneFamily.Unknown.ToString() }; 
                EntityRunes ewok = new EntityRunes { Name = "Ewok", Icon = "Inconnu", Description = "Inconnu", Family = RuneFamily.Unknown.ToString() };

                context.Add(tmpImage);
                context.Add(tmpImage2);
                context.Add(tmpImage3);
                chewie.ImageId = tmpImage.Id;
                yoda.ImageId = tmpImage2.Id;
                ewok.ImageId = tmpImage3.Id;
                context.Add(chewie);
                context.Add(yoda);
                context.Add(ewok);
                context.SaveChanges();
            }
        }


        [Fact]
        public void Add_Test()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<BDDContext>()
                .UseSqlite(connection)
                .Options;

            Add_Champion_Rune(options);

            //uses another instance of the context to do the tests
            using (var context = new BDDContext(options))
            {
                context.Database.EnsureCreated();

                Assert.Equal(3, context.Runes.Count());
                Assert.Equal("Yoda", context.Runes.OrderBy(e => e.Name).Last().Name);
            }
        }

        [Fact]
        public void Modify_Test()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<BDDContext>()
                .UseSqlite(connection)
                .Options;

            Add_Champion_Rune(options);

            //uses another instance of the context to do the tests
            using (var context = new BDDContext(options))
            {
                context.Database.EnsureCreated();

                string nameToFind = "ew";
                Assert.Equal(2, context.Runes.Where(n => n.Name.ToLower().Contains(nameToFind)).Count());
                nameToFind = "wo";
                Assert.Equal(1, context.Runes.Where(n => n.Name.ToLower().Contains(nameToFind)).Count());
                var ewok = context.Runes.Where(n => n.Name.ToLower().Contains(nameToFind)).First();
                ewok.Name = "Wicket";
                context.SaveChanges();
            }

            //uses another instance of the context to do the tests
            using (var context = new BDDContext(options))
            {
                context.Database.EnsureCreated();

                string nameToFind = "ew";
                Assert.Equal(1, context.Runes.Where(n => n.Name.ToLower().Contains(nameToFind)).Count());
                nameToFind = "wick";
                Assert.Equal(1, context.Runes.Where(n => n.Name.ToLower().Contains(nameToFind)).Count());
            }
        }

        [Fact]
        public void Delete_Test()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<BDDContext>()
                .UseSqlite(connection)
                .Options;

            Add_Champion_Rune(options);

            using (var context = new BDDContext(options))
            {
                EntityRunes tmpChamp = context.Runes.OrderBy(e => e.Name).Include(e => e.Image).Last();

                context.Remove(tmpChamp);
                context.SaveChanges();
            }

            using (var context = new BDDContext(options))
            {
                Assert.Equal(2, context.Runes.Count());
                Assert.Equal("Ewok", context.Runes.OrderBy(e => e.Name).Last().Name);
            }
        }
    }
}
