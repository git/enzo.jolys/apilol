﻿using DTO;
using EntityFramwork;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestUnitaireBDD
{
    public class TestSkill
    {
        private void Add_Skill(DbContextOptions<BDDContext> options)
        {
            
            //Image Champ
            EntityLargeImage imageChamp = new EntityLargeImage();
            imageChamp.Base64 = "Inconnu";
            imageChamp.Id = 99;

            //Champion
            EntityChampions champ = new EntityChampions { Name = "Chewbacca", Bio = "Inconnu", Icon = "Inconnu", Classe = ChampionClass.Assassin.ToString() };
            champ.ImageId = imageChamp.Id;
            champ.Id = 99;

            //prepares the database with one instance of the context
            using (var context = new BDDContext(options))
            {
                //context.Database.OpenConnection();    
                context.Database.EnsureCreated();
                context.Add(imageChamp);
                context.Add(champ);
                EntitySkill skill = new EntitySkill { Name = "toto", Description = "Inconnu", Type = SkillType.Unknown.ToString(),ChampionId = champ.Id,Id = 1 };
                context.Add(skill);

                context.SaveChanges();
            }
        }

        [Fact]
        public void Add_Test()
        {
            
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<BDDContext>()
                .UseSqlite(connection)
                .Options;

            Add_Skill(options);

            //uses another instance of the context to do the tests
            using (var context = new BDDContext(options))
            {
                context.Database.EnsureCreated();

                Assert.Equal(1, context.Skills.Count());
                Assert.Equal("toto", context.Skills.First().Name);
            }
        }

        [Fact]
        public void Modify_Test()
        {
            
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<BDDContext>()
                .UseSqlite(connection)
                .Options;

            Add_Skill(options);

            //uses another instance of the context to do the tests
            using (var context = new BDDContext(options))
            {
                var tmpSkill = context.Skills.First();
                tmpSkill.Name = "totoModify";
                context.SaveChanges();
            }

            //uses another instance of the context to do the tests
            using (var context = new BDDContext(options))
            {
                Assert.Equal("totoModify", context.Skills.First().Name);
            }
            Assert.True(true);
        }

        [Fact]
        public void Delete_Test()
        {
            
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<BDDContext>()
                .UseSqlite(connection)
                .Options;

            Add_Skill(options);

            using (var context = new BDDContext(options))
            {
                var tmp = context.Skills.First();

                context.Remove(tmp);
                context.SaveChanges();
            }

            using (var context = new BDDContext(options))
            {
                Assert.Equal(0, context.Skills.Count());
            }
            
            Assert.True(true);
        }
    }
}
