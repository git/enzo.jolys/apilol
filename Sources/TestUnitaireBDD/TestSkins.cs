﻿using DTO;
using EntityFramwork;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestUnitaireBDD
{
    public class TestSkins
    {
        private void Add_Skin(DbContextOptions<BDDContext> options)
        {
            
            //Image Champ
            EntityLargeImage imageChamp = new EntityLargeImage();
            imageChamp.Base64 = "Inconnu";
            imageChamp.Id = 95;

            //Image skin 
            EntityLargeImage imageSkin = new EntityLargeImage();
            imageSkin.Base64 = "Inconnu";
            imageSkin.Id = 96;
            

            //prepares the database with one instance of the context
            using (var context = new BDDContext(options))
            {
                //context.Database.OpenConnection();    
                context.Database.EnsureCreated();

                EntityChampions chewie = new EntityChampions { Name = "Chewbacca", Bio = "Inconnu", Icon = "Inconnu", Classe = ChampionClass.Assassin.ToString() };
                EntitySkins skin = new EntitySkins { Name = "toto",Description = "Inconnu", Icon = "Inconnu", Price = 1350 };

                context.Add(imageChamp);
                context.Add(imageSkin);

                chewie.ImageId = imageChamp.Id;
                context.Add(chewie);

                skin.ImageId = 2;
                skin.ChampionId = 1;

                context.Add(skin);
                
                context.SaveChanges();
            }
        }

        [Fact]
        public void Add_Test()
        {
            
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<BDDContext>()
                .UseSqlite(connection)
                .Options;

            Add_Skin(options);

            //uses another instance of the context to do the tests
            using (var context = new BDDContext(options))
            {
                context.Database.EnsureCreated();

                Assert.Equal(18, context.Skins.Count());
                Assert.Equal(1820, context.Skins.First().Price);
            }
        }

        [Fact]
        public void Modify_Test()
        {
            
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<BDDContext>()
                .UseSqlite(connection)
                .Options;

            Add_Skin(options);

            //uses another instance of the context to do the tests
            using (var context = new BDDContext(options))
            {
                var tmpSkin = context.Skins.First();
                tmpSkin.Price = 100;
                context.SaveChanges();
            }

            //uses another instance of the context to do the tests
            using (var context = new BDDContext(options))
            {
               Assert.Equal(100,context.Skins.First().Price);
            }
        }

        [Fact]
        public void Delete_Test()
        {
            
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<BDDContext>()
                .UseSqlite(connection)
                .Options;

            Add_Skin(options);

            using (var context = new BDDContext(options))
            {
                var tmp = context.Skins.First();

                context.Remove(tmp);
                context.SaveChanges();
            }

            using (var context = new BDDContext(options))
            {
                Assert.Equal(17, context.Skins.Count());
            }
        }
    }
}
